;;; Module: calcurse

;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (calcurse)
  #:use-module (guix packages)
  #:use-module (guix licenses)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module (gnu packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages pkg-config)  
  #:use-module (gnu packages base)
  #:use-module (gnu packages ncurses))

(use-modules ((gnu packages gettext)))
(define-public calcurse
  (package
    (name "calcurse")
    (version "4.8.0")
   (source (origin
            ;;(method url-fetch)
	    (method git-fetch)
            ;;(uri (string-append "file://" (getenv "HOME") "/src/calcurse"))
	    (uri (git-reference
	          (commit "c087e99ae73fe8b185051410e8aed1004ca07216")
	          (url "https://github.com/methuselah-0/calcurse.git")))
            (sha256
             (base32
              "1p7x1f52s4902fn0sqihqpf6aqwnl5m4lbkb22vaw9azrmcrgpq7"))))
    (build-system gnu-build-system)
    (inputs (list ncurses))
    (native-inputs (list tzdata-for-tests autoconf-2.71 automake gettext-minimal autoconf-archive pkg-config asciidoc)) ;; gettext for autopoint, autoconf-archive for AX_WITH_CURSES
    (arguments
     ;; The ical tests all want to create a ".calcurse" directory, and may
     ;; fail with "cannot create directory '.calcurse': File exists" if run
     ;; concurrently.
     `(#:configure-flags
       (list (string-append "--docdir=" (assoc-ref %outputs "out")
                            "/share/doc/" ,name "-" ,version))
       #:parallel-tests? #f
       #:tests? #f ;; 2 tests fail, dunno why
       ;; Since this tzdata is only used for tests and not referenced by the
       ;; built package, used the "fixed" obsolete version of tzdata and ensure
       ;; it does not sneak in to the closure.
       #:disallowed-references (,tzdata-for-tests)))
    (home-page "https://www.calcurse.org")
    (synopsis "Text-based calendar and scheduling")
    (description
     "Calcurse is a text-based calendar and scheduling application.  It helps
keep track of events, appointments and everyday tasks.  A configurable
notification system reminds user of upcoming deadlines, and the curses based
interface can be customized to suit user needs.  All of the commands are
documented within an online help system.")
    (license bsd-2)))
calcurse
