(define-module (linuxpro)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system asdf)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system haskell)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system perl)
  #:use-module (guix build-system python)
  #:use-module (guix build-system trivial)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages aidc)  
  #:use-module (gnu packages bash)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages calendar)
  #:use-module (gnu packages check)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages datastructures)
  #:use-module (gnu packages docbook)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages fribidi)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gperf)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages haskell-check)
  #:use-module (gnu packages haskell-web)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages image)
  #:use-module (gnu packages imagemagick)
  #:use-module (gnu packages javascript)  
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages lisp-check)
  #:use-module (gnu packages lisp-xyz)
  #:use-module (gnu packages logging)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages man)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpd)
  #:use-module (gnu packages networking)  
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pretty-print)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages readline)
  #:use-module (gnu packages sdl)  
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages suckless)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages textutils)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages time)
  #:use-module (gnu packages video)
  #:use-module (gnu packages web)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages ninja)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xorg))

(define-public sov
  (package
    (name "sov")
    (version "0.92b")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/milgra/sov/archive/refs/tags/" version ".tar.gz"))
              (sha256
               (base32
                "17gszhrvsv7snkww0s73pzyz7gws9yayb6fc9dm046s8mx9lcyya"))))
    (build-system meson-build-system)
    ;; test says no writable cache directories
    (arguments `(#:tests? #f))
    (inputs (list libpng freetype glew egl-wayland wayland wayland-protocols libxkbcommon font-terminus fontconfig))
    ;; wayland-client -> wayland
    ;; libegl -> egl-wayland
    ;; TODO: libgl,
    (native-inputs (list ninja meson pkg-config))
    (home-page "tobe")
    (synopsis "tobe")
    (description "tobe")
    (license license:gpl3)))

;; (define-public mupdf-with-third
;;   (package (inherit mupdf)
;;            (version "1.17.0")
;;            (source
;;             (origin
;;              (method url-fetch)
;;              (uri (string-append "https://mupdf.com/downloads/archive/"
;;                                  "mupdf-" version "-source.tar.gz"))
;;              (sha256
;;               (base32 "13nl9nrcx2awz9l83mlv2psi1lmn3hdnfwxvwgwiwbxlkjl3zqq0"))))))
(define-public mupdf-with-third
  (package
    (name "mupdf")
    (version "1.16.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://mupdf.com/downloads/archive/"
                           "mupdf-" version "-source.tar.xz"))
       (sha256
        (base32 "1npmy92lkj41nnc14b4fpq7z62pminy94zsdbrczj22jpn283rvg"))
       (modules '((guix build utils)))
       (snippet
        '(begin
           ;; Remove bundled software.
           (let* ((keep (list "lcms2")) ; different from our lcms2 package
                  (from "thirdparty")
                  (kept (string-append from "~temp")))
             (mkdir-p kept)
             (for-each (lambda (file) (rename-file (string-append from "/" file)
                                              (string-append kept "/" file)))
                       keep)
             (delete-file-recursively from)
             (rename-file kept from))
           #t))))
    (build-system gnu-build-system)
    (inputs
      `(("curl" ,curl)
        ("freeglut" ,freeglut)
        ("freetype" ,freetype)
        ("harfbuzz" ,harfbuzz)
        ("jbig2dec" ,jbig2dec)
        ("libjpeg" ,libjpeg-turbo)
        ("libx11" ,libx11)
        ("libxext" ,libxext)
        ("mujs" ,mujs)
        ("openjpeg" ,openjpeg)
        ("openssl" ,openssl)
        ("zlib" ,zlib)))
    (native-inputs
      `(("pkg-config" ,pkg-config)))
    (arguments
      '(#:tests? #f ; no check target
        #:make-flags (list "CC=gcc"
                           "XCFLAGS=-fpic"
                           "USE_SYSTEM_LIBS=yes"
                           "USE_SYSTEM_MUJS=yes"
                           (string-append "prefix=" (assoc-ref %outputs "out")))
        #:phases (modify-phases %standard-phases
                  (delete 'configure))))
    (home-page "https://mupdf.com")
    (synopsis "Lightweight PDF viewer and toolkit")
    (description
      "MuPDF is a C library that implements a PDF and XPS parsing and
rendering engine.  It is used primarily to render pages into bitmaps,
but also provides support for other operations such as searching and
listing the table of contents and hyperlinks.

The library ships with a rudimentary X11 viewer, and a set of command
line tools for batch rendering @command{pdfdraw}, rewriting files
@command{pdfclean}, and examining the file structure @command{pdfshow}.")
    (license (list license:agpl3+
                   license:bsd-3 ; resources/cmaps
                   license:x11 ; thirdparty/lcms2
                   license:silofl1.1 ; resources/fonts/{han,noto,sil,urw}
                   license:asl2.0)))) ; resources/fonts/droid

(define-public mmfm
  (package
   (name "mmfm")
   (version "0.53b")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/milgra/mmfm/archive/refs/tags/" version ".tar.gz"))
            (sha256
             (base32
              "0snbz49n44n2zh5q4iysypsjvhpjsw2n1010f0pw6p5g0bg31iha"))))
   (build-system meson-build-system)
   ;; test says no writable cache directories
   (arguments `(#:tests? #f)) ;; fails cuz XDG_RUNTIME_DIR isnt set
   (inputs (list libpng freetype glew egl-wayland wayland wayland-protocols libxkbcommon font-terminus fontconfig mupdf-with-third mujs sdl2 ijg-libjpeg jbig2dec openjpeg harfbuzz gumbo-parser ffmpeg))
   ;; wayland-client -> wayland
   ;; libegl -> egl-wayland
   ;; TODO: libgl,
   (native-inputs (list ninja meson pkg-config))
   (home-page "tobe")
   (synopsis "tobe")
   (description "tobe")
   (license license:gpl3)))

(define-public vmp
  (package
   (name "vmp")
   (version "0.92b")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/milgra/vmp/archive/refs/tags/" version ".tar.gz"))
            (sha256
             (base32
              "05lybl9c914jlklqr7q8fmk594k2lismjifc1qxfzwr0by8x60lq"))))
   (build-system meson-build-system)
   ;; test says no writable cache directories
   (arguments `(#:tests? #f)) ;; fails cuz XDG_RUNTIME_DIR isnt set
   (inputs (list libpng freetype glew egl-wayland wayland wayland-protocols libxkbcommon font-terminus fontconfig mupdf-with-third mujs sdl2 ijg-libjpeg jbig2dec openjpeg harfbuzz gumbo-parser ffmpeg))
   ;; wayland-client -> wayland
   ;; libegl -> egl-wayland
   ;; TODO: libgl,
   (native-inputs (list ninja meson pkg-config))
   (home-page "tobe")
   (synopsis "tobe")
   (description "tobe")
   (license license:gpl3)))

(define-public wcp
  (package
   (name "wcp")
   (version "0.77b")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/milgra/wcp/archive/refs/tags/" version ".tar.gz"))
            (sha256
             (base32
              "1np7vqs3ag2hnw597r0bfvmp71w10dmlwsfrc81wsbcy2r0fx2x8"))))
   (build-system meson-build-system)
   ;; test says no writable cache directories
   (arguments `(#:tests? #f)) ;; fails cuz XDG_RUNTIME_DIR isnt set
   (inputs (list libpng freetype glew egl-wayland wayland wayland-protocols libxkbcommon font-terminus fontconfig mupdf-with-third mujs sdl2 ijg-libjpeg jbig2dec openjpeg harfbuzz gumbo-parser ffmpeg))
   ;; wayland-client -> wayland
   ;; libegl -> egl-wayland
   ;; TODO: libgl,
   (native-inputs (list ninja meson pkg-config))
   (home-page "tobe")
   (synopsis "tobe")
   (description "tobe")
   (license license:gpl3)))

(define-public iwgtk
  (package
   (name "iwgtk")
   (version "v0.9")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/J-Lentz/iwgtk/archive/refs/tags/" version ".tar.gz"))
            (sha256
             (base32
              "0c9xb08g83qp75s55nql84zkbhbmx508h52xlqs60lzy633jva44"))))
   (build-system meson-build-system)
   ;; test says no writable cache directories
   (arguments `(#:tests? #f)) ;; fails cuz XDG_RUNTIME_DIR isnt set
   (inputs (list iwd gtk scdoc qrencode adwaita-icon-theme))
   ;; wayland-client -> wayland
   ;; libegl -> egl-wayland
   ;; TODO: libgl,
   (native-inputs (list ninja meson pkg-config))
   (home-page "tobe")
   (synopsis "tobe")
   (description "tobe")
   (license license:gpl3)))


;;sov
;;mmfm
;;mupdf-with-third
;;vmp
;;wcp ;; actually needs runtime: awk, sed, bluez (bluetoothctl), pamixer, brightnessctl, iwd (iwctl)
;;iwgtk
