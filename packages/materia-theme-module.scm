(define-module (materia-theme-module)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages web))

(define-public materia-theme-transparent
  (package
   (name "materia-theme-transparent")
   (version "20210322")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/methuselah-0/materia-theme-transparent.git")
           (commit
	    "81bbfe6ca61e0b372daa7fada4c2d85752925cfd")))
     (file-name (git-file-name name version))
     ;;(patches (list (string-append (dirname (current-filename)) "/0001-transparent.patch")))
     (sha256
      (base32
       "0jv2h78qc1qh83qcrkzlvy0k961yv8688xdsvk4nxcy1999123nr"))))
   (build-system meson-build-system)
   (native-inputs
    (list gtk+ sassc))
   (home-page "https://github.com/nana-4/materia-theme")
   (synopsis "Material Design theme for a wide range of environments")
   (description "Materia is a Material Design theme for GNOME/GTK based
desktop environments.  It supports GTK 2, GTK 3, GNOME Shell, Budgie,
Cinnamon, MATE, Unity, Xfce, LightDM, GDM, Chrome theme, etc.")
   (license license:gpl2+)))
materia-theme-transparent
