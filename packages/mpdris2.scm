(define-module (mpdris2)
  #:use-module (gnu packages)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages aidc)
  #:use-module (gnu packages assembly);; in addition to crypto.scm mods
  #:use-module (gnu packages attr)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages check)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages cryptsetup)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages graphviz)
  #:use-module (gnu packages image)
  #:use-module (gnu packages kerberos)
  #:use-module (gnu packages libbsd)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages logging)
  #:use-module (gnu packages lsof)
  #:use-module (gnu packages man)
  #:use-module (gnu packages mpd)
  #:use-module (gnu packages multiprecision)
  #:use-module (gnu packages nettle)
  #:use-module (gnu packages password-utils)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages perl-check)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages readline)
  #:use-module (gnu packages search)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages wxwidgets);; in addition to crypto.scm mods
  #:use-module (gnu packages xml)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (guix build-system cargo)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system go)
  #:use-module (guix build-system perl)
  #:use-module (guix build-system python)
  #:use-module (guix utils)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26))

(define-public mpdris2
  (package
    (name "mpdris2")
    (version "0.9.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://github.com/eonpatapon/mpDris2/archive/refs/tags/"
             version ".tar.gz"))
       (sha256
        (base32 "1rmybxv0nrr6v4alhngxcim7g72a6dii0jplrw8671p785w4dw6h"))))
    (build-system gnu-build-system)
    (arguments
      '(;; #:phases
        ;; (modify-phases %standard-phases
        ;;   (delete 'configure)
        ;;   (add-before 'build 'build-env
        ;;     (lambda* (#:key outputs #:allow-other-keys)
        ;;       (chdir "src")
        ;;       (setenv "CC" "gcc")
        ;;       (setenv "DESTDIR" (assoc-ref outputs "out"))
        ;;       #t))
        ;;   (add-after 'install 'fix-install
        ;;     (lambda* (#:key outputs version #:allow-other-keys)
        ;;       (let ((out (assoc-ref outputs "out")))
        ;;         (install-file (string-append out "/usr/bin/veracrypt") (string-append out "/bin"))
        ;;         (copy-recursively (string-append out "/usr/share/applications") (string-append out "/share/applications"))
        ;;         (copy-recursively (string-append out "/usr/share/pixmaps") (string-append out "/share/pixmaps"))
        ;;         (copy-recursively (string-append out "/usr/share/doc/veracrypt/HTML") (string-append out "/share/doc/veracrypt-1.24/HTML"))
        ;;         (delete-file-recursively (string-append out "/usr"))))))
        ;; ; Veracrypt only has a test suite for Windows.
        #:tests? #f))
    (native-inputs (list autoconf automake which git intltool python))
    (inputs (list python-mpd2))
    (propagated-inputs (list python-mpd2 python-dbus))
    ;; (native-inputs
    ;;  `(("pkg-config" ,pkg-config)
    ;;    ("yasm" ,yasm)))
    ;; (inputs
    ;;   `(("fuse" ,fuse)
    ;;     ("wxwidgets" ,wxwidgets)))
    ;; (propagated-inputs
    ;;   `(("lvm2" ,lvm2)))
    (home-page "https://www.veracrypt.fr/")
    (synopsis "Disk encryption software")
    (description "VeraCrypt is a free open source disk encryption software
based on TrueCrypt 7.1a.")
    (license license:asl2.0)))
mpdris2
