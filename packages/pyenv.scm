(define-module (pyenv)
  #:use-module (gnu packages) ;; because of complaints in /var/log/cuirass/evaluations/1.gz
  #:use-module (gnu packages base) ;; because of findutils
  #:use-module (gnu packages compression) ;; because of zlib
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages time) ;; python-pytz is here
  #:use-module (gnu packages tcl) ;; tk
  #:use-module (gnu packages python-check) ;; python-coveralls is here
  #:use-module (gnu packages databases) ;; python-sqlalchemy is here
  #:use-module (gnu packages flex)
  #:use-module (gnu packages python) ;; python-testpath to figure out python version, ;; because of complaints in /var/log/cuirass/evaluations/1.gz
  #:use-module (gnu packages check) ;; python-pytest
  #:use-module (gnu packages libffi) ;; libffi!
  #:use-module (gnu packages monitoring) ;; python-prometheus-client
  #:use-module (gnu packages ncurses) ;; ncurses!
  #:use-module (gnu packages xml) ;; xmlsec, libxml2
  #:use-module (gnu packages python-web) ;; python-terminado
  #:use-module (gnu packages readline) ;; readline!
  #:use-module (gnu packages serialization) ;; python-sphinx  
  #:use-module (gnu packages sphinx) ;; python-sphinx
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages textutils) ;; python-pandocfilters
  #:use-module (gnu packages tls)
  #:use-module (gnu packages python-crypto) ;; python-certifi
  #:use-module (guix build-system python)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages graphviz)
  #:use-module (gnu packages moreutils)
  #:use-module (gnu packages ruby)
  #:use-module (gnu packages libffi)
  ;;#:use-module (bcu-channel python-extras-2)
  #:use-module ((guix licenses) #:prefix license:))

(define-public python-pyenv
  (package
    (name "python-pyenv")
    (version "2.3.35")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/pyenv/pyenv/archive/refs/tags/v" version ".tar.gz"))
       (sha256
        (base32 "1z07ayiv9pf54k1ryxn3bjww58w11lhng549lr2gw9j1qfhl7xfc"))))
    (build-system gnu-build-system)
    (arguments
     (list
      #:tests? #f
      #:phases
       #~(modify-phases %standard-phases
           (add-before 'configure 'change-to-src
               ;;(lambda _ (invoke "cd" "./src"))))))
             (lambda _ (chdir "src")))
           (replace 'install
               ;;(lambda _ (invoke "cd" "./src"))))))

             (lambda* (#:key outputs #:allow-other-keys)
               (let ((out (assoc-ref outputs "out")))
                 (copy-recursively "../bin" (string-append out "/bin"))
                 (copy-recursively "../plugins" (string-append out "/plugins"))
                 (copy-recursively "../man" (string-append out "/usr/share/"))
                 (copy-recursively "../libexec" (string-append out "/libexec"))
                 #t))))))
    ;; (native-inputs
    ;;  (list python-pytest python-pytest-mock))
    (propagated-inputs
     (list ;;openssl ;; not propagating since that's annoying but might be useful
           `(,zlib "static")
           bzip2
           lbzip2
           readline
           `(,sqlite "static")
           curl
           ncurses
           xz
           tk
           tcl
           `(,libxml2 "static")
           xmlsec
           libffi
           lzlib)) ;; build-essential libssl-dev
;; zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev curl
;; libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev
;; liblzma-dev
    (home-page "https://github.com/pyenv/pyenv")
    (synopsis "Install and switch between multiple Python versions")
    (description
     "pyenv lets you easily switch between multiple versions of Python. It's simple, unobtrusive, and follows the UNIX tradition of single-purpose tools that do one thing well.")
    (license license:expat)))
python-pyenv
