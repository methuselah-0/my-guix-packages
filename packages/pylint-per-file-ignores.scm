;;; Module: pylint-per-file-ignores
;;; It adds the package pylint-per-file-ignores
;;; This module is modified from Guix gnu/packages/python-xyz.
;;; changes are mainly modified package versions

;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (pylint-per-file-ignores)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages adns)
  #:use-module (gnu packages aidc)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages attr)
  #:use-module (gnu packages backup)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages bdw-gc)
  #:use-module (gnu packages check)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages dbm)
  #:use-module (gnu packages digest)
  #:use-module (gnu packages django)
  #:use-module (gnu packages djvu)
  #:use-module (gnu packages docker)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages enchant)
  #:use-module (gnu packages file)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gdb)
  #:use-module (gnu packages geo)
  #:use-module (gnu packages ghostscript)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages graphics)
  #:use-module (gnu packages graphviz)
  #:use-module (gnu packages gsasl)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages icu4c)
  #:use-module (gnu packages image)
  #:use-module (gnu packages image-processing)
  #:use-module (gnu packages imagemagick)
  #:use-module (gnu packages inkscape)
  #:use-module (gnu packages java)
  #:use-module (gnu packages jupyter)
  #:use-module (gnu packages kerberos)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages libidn)
  #:use-module (gnu packages libusb)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages machine-learning)
  #:use-module (gnu packages man)
  #:use-module (gnu packages markup)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages monitoring)
  #:use-module (gnu packages multiprecision)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages ninja)
  #:use-module (gnu packages node)
  #:use-module (gnu packages openstack)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages photo)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages protobuf)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-compression)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages rdf)
  #:use-module (gnu packages readline)
  #:use-module (gnu packages regex)
  #:use-module (gnu packages scanner)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages search)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages swig)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages terminals)
  #:use-module (gnu packages tex)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages textutils)
  #:use-module (gnu packages time)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages video)
  #:use-module (gnu packages web)
  #:use-module (gnu packages wxwidgets)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xorg)
  #:use-module (guix packages)
  #:use-module (guix build-system cargo)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (guix download)
  #:use-module (guix hg-download)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26))

(use-modules (guix packages) (guix download) (gnu packages python-build) (guix build-system python) (guix build-system pyproject) (guix licenses) ((guix licenses) #:prefix license:) (gnu packages python-xyz))

(define-public python-virtualenv
  (package
    (name "python-virtualenv")
    (version "20.4.7")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "virtualenv" version))
       (sha256
        (base32
         "0rxl180hc5igz8am8pvayxf2mvknsisriakcxfj2kfqdz14ziz8l"))))
    (build-system python-build-system)
    (native-inputs
     (list python-mock python-pytest python-setuptools-scm))
    (propagated-inputs
     (list python-appdirs python-distlib python-filelock python-six))
    (home-page "https://virtualenv.pypa.io/")
    (synopsis "Virtual Python environment builder")
    (description
     "Virtualenv is a tool to create isolated Python environments.")
    (license license:expat)))

(define-public poetry
  (package
    (name "poetry")
    (version "1.4.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "poetry" version))
       (sha256
        (base32
         "0g0vczn6qa4b2bdkq4k7fm1g739vyxp2iiblwwsrcmw24jj81m8b"))))
    (build-system pyproject-build-system)
    (arguments
     `(#:tests? #f                      ;PyPI does not have tests
        #:phases
        (modify-phases %standard-phases
		(delete 'sanity-check)))) ;; because it thinks trove-classifiers is the wrong version when it isn't
       ;; #:phases
       ;; (modify-phases %standard-phases
       ;;   (add-before 'build 'patch-setup-py
       ;;     (lambda _
       ;;       (substitute* "setup.py"
       ;;         ;; Relax some of the requirements.
       ;;         (("(keyring>=21.2.0),<22.0.0" _ keyring) keyring)
       ;;         (("(packaging>=20.4),<21.0" _ packaging) packaging)))))
    (propagated-inputs
     (list python-cachecontrol
           python-cachy
           python-cleo
           python-crashtest
           python-entrypoints
           python-html5lib
           python-keyring
           python-msgpack
           python-packaging
           python-pexpect
           python-pip
           python-pkginfo
           python-poetry-core
           python-requests
           python-requests-toolbelt
           python-shellingham
           python-trove-classifiers
           python-tomlkit
           python-virtualenv))
    (home-page "https://python-poetry.org")
    (synopsis "Python dependency management and packaging made easy")
    (description "Poetry is a tool for dependency management and packaging
in Python.  It allows you to declare the libraries your project depends on and
it will manage (install/update) them for you.")
    (license license:expat)))

(define-public python-pylint-per-file-ignores
  (package
   (name "python-pylint-per-file-ignores")
   (version "1.3.0")
   (source (origin
            ;;(method url-fetch)
	    (method git-fetch)
            ;;(uri (pypi-uri "pylint_per_file_ignores" version))
	    (uri (git-reference
		  (commit "b3da7146f6bf7acccc37475ccf13ee44b1ea570a")
		  (url "https://github.com/danie1k/pylint-per-file-ignores.git")))
            (sha256
             (base32
              "03dzi8k2nkp1hp22d5a9sxjfbzickxf10mfn4y5adq9q2d7ylcwb"))))
   (build-system pyproject-build-system)
   (arguments `(#:tests? #f))
   (propagated-inputs (list python-tomli poetry))
   (home-page
    "https://github.com/christopherpickering/pylint-per-file-ignores.git")
   (synopsis "A pylint plugin to ignore error codes per file.")
   (description
    "This package provides a pylint plugin to ignore error codes per file.")
   (license license:expat)))
python-pylint-per-file-ignores
