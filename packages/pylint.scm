;;; Module: pylint
;;; It adds a more recent python-pylint package.
;;;
;;; This module is modified from Guix gnu/packages/python-xyz.
;;; changes are mainly modified package versions
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (pylint)
  #:use-module (gnu packages)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages guile-xyz)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages time)
  #:use-module (gnu packages xml)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system go)
  #:use-module (guix build-system guile)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (guix build-system trivial)
  #:use-module (guix deprecation)
  #:use-module (gnu packages check)
  #:use-module (srfi srfi-1))

(define-public python-astroid
  (package
    (name "python-astroid")
    (version "2.15.6")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/PyCQA/astroid")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0n08hiv02swrd9465vi433ph9k2wzr01jrsx6fj83bgwa08lv0yj"))))
    (build-system pyproject-build-system)
    ;; one test fails, 1479passed
    (arguments `(#:tests? #f))
    (propagated-inputs
     (list python-lazy-object-proxy python-typing-extensions python-wrapt))
    (native-inputs
     (list python-pytest python-pytest-runner))
    (home-page "https://github.com/PyCQA/astroid")
    (synopsis "Python source code base representation")
    (description "@code{python-astroid} provides a common base representation
of Python source code for projects such as pychecker, pyreverse, pylint, etc.
It provides a compatible representation which comes from the _ast module.  It
rebuilds the tree generated by the builtin _ast module by recursively walking
down the AST and building an extended ast.  The new node classes have
additional methods and attributes for different usages.  They include some
support for static inference and local name scopes.  Furthermore, astroid
builds partial trees by inspecting living objects.")
    (license license:lgpl2.1+)))

(define-public python-pylint
  (package
    (name "python-pylint")
    (version "2.17.4")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/PyCQA/pylint")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0ippkvb50a0b0cp67pd9c9fm4dgy89dr5j4fr9imhxypnrvrxvc6"))))
    (build-system pyproject-build-system)
    (arguments `(#:tests? #f))
    ;; (arguments
    ;;  `(#:phases
    ;;    (modify-phases %standard-phases
    ;;      (replace 'check
    ;;        (lambda* (#:key tests? #:allow-other-keys)
    ;;          (when tests?
    ;;            ;; The unused but collected 'primer'-related test files require
    ;;            ;; the extraneous 'git' Python module; remove them.
    ;;            ;;(delete-file "tests/primer/test_primer_external.py")
    ;;            ;;(delete-file "tests/testutils/test_package_to_lint.py")
    ;;            (setenv "HOME" "/tmp")
    ;;            (invoke "pytest" "-k" "test_functional"
    ;;                    "-n" (number->string (parallel-job-count)))))))))
    (native-inputs
     (list python-pytest python-pytest-xdist))
    (propagated-inputs
     (list python-astroid
           python-dill
           python-isort
           python-mccabe
           python-platformdirs
           python-tomlkit
           python-typing-extensions))
    (home-page "https://github.com/PyCQA/pylint")
    (synopsis "Advanced Python code static checker")
    (description "Pylint is a Python source code analyzer which looks
for programming errors, helps enforcing a coding standard and sniffs
for some code smells (as defined in Martin Fowler's Refactoring book).

Pylint has many rules enabled by default, way too much to silence them
all on a minimally sized program.  It's highly configurable and handle
pragmas to control it from within your code.  Additionally, it is
possible to write plugins to add your own checks.")
    (license license:gpl2+)))

(define-public nwg-panel
  (package
    (name "nwg-panel")
    (version "0.9.10")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://github.com/nwg-piotr/nwg-panel/archive/refs/tags/v"
             version ".tar.gz"))
       (sha256
        (base32 "11pq5an03c98jfql2kgmqqz2g38g7gan4d0js3k65dh9j87lnp34"))))
    (build-system python-build-system)
    ;;(arguments `(#:tests? #f))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (delete 'check) ; no tests
         (delete 'sanity-check) ;; namespace gtk not available
         )))
         ;; (replace 'sanity-check
         ;;   (let ((sanity-check (assoc-ref %standard-phases 'sanity-check)))
         ;;     (lambda* (#:key inputs outputs #:allow-other-keys #:rest args)
         ;;       (setenv "HOME" "/tmp")
         ;;       (setenv "GI_TYPELIB_PATH" "build/src")
         ;;       (setenv "LD_LIBRARY_PATH" "build/src")
         ;;       (apply sanity-check args))))
;;          (replace 'install
;;            (lambda* (#:key inputs outputs  #:allow-other-keys)
;; ;;             (setenv "HOME" "/tmp")
;;              (invoke "./install.sh" (string-append "--prefix=" (assoc-ref %outputs "out"))))))))
    (native-inputs
     (list bash-minimal python-pytest python-pytest-xdist gtk))
    ;; TODO: this list can almost definitely be reduced.
    (propagated-inputs
     (list python-pygobject python-psutil python-i3ipc gobject-introspection python-gipc glib gtk+ python-importlib-metadata python-setuptools gtk-layer-shell python-pycairo brightnessctl))
    (home-page "https://github.com/PyCQA/pylint")
    (synopsis "Advanced Python code static checker")
    (description "Pylint is a Python source code analyzer which looks
for programming errors, helps enforcing a coding standard and sniffs
for some code smells (as defined in Martin Fowler's Refactoring book).

Pylint has many rules enabled by default, way too much to silence them
all on a minimally sized program.  It's highly configurable and handle
pragmas to control it from within your code.  Additionally, it is
possible to write plugins to add your own checks.")
    (license license:gpl2+)))
;;python-pylint
