(define-module (python-holidays))
(use-modules (guix packages) (guix download) (gnu packages python-build) (guix build-system python) (guix build-system pyproject) (guix licenses) ((guix licenses) #:prefix license:) (gnu packages python-xyz) (gnu packages time) (gnu packages check))
(define-public python-holidays
  (package
  (name "python-holidays")
  (version "0.29")
  (source (origin
            (method url-fetch)
            (uri (pypi-uri "holidays" version))
            (sha256
             (base32
              "0rqabf10f3qinj36xy7d5zfv0c3nyn9xmxsg4hbjzn8dazqrs8g8"))))
  (build-system pyproject-build-system)
  (native-inputs (list python-pytest))
  (propagated-inputs (list python-dateutil))
  (home-page "https://github.com/dr-prodigy/python-holidays")
  (synopsis "Generate and work with holidays in Python")
  (description "Generate and work with holidays in Python")
  (license license:expat))
)
python-holidays
