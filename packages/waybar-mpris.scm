(define-module (waybar-mpris)
  #:use-module (gnu packages golang)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (guix memoization)
  #:use-module ((guix build utils) #:select (alist-replace))
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system go)
  #:use-module (gnu packages)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages base)
  #:use-module ((gnu packages bootstrap) #:select (glibc-dynamic-linker))
  #:use-module (gnu packages check)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages mail)
  #:use-module (gnu packages mp3)
  #:use-module (gnu packages password-utils)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages ruby)
  #:use-module (gnu packages syncthing)
  #:use-module (gnu packages terminals)
  #:use-module (gnu packages textutils)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages web)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1))

(define-public go-github-com-arafatamim-mpris2client
  (package
    (name "go-github-com-arafatamim-mpris2client")
    (version "0.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/arafatamim/mpris2client.git")
                    (commit "0d6c121e868f31198a28be6efe32a484f6060ae9")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0z73ivr7dalmpmyq1lhjgq6r4k3hvbz2b1zk3b6qmqpm8f3v9wdx"))))
    (build-system go-build-system)
    (propagated-inputs (list go-github-com-godbus-dbus))
    (arguments
     (list #:import-path "github.com/arafatamim/mpris2client"
           #:tests? #f ; Source-only package.
           ;; #:phases
           ;; #~(modify-phases %standard-phases
           ;;     ;; Source-only package.
           ;;     (delete 'build))
           ))
    (home-page "https://github.com/lithammer/fuzzysearch")
    (synopsis "Tiny and fast fuzzy search in Go")
    (description
     "A speedy fuzzy matching package for Go inspired by the JavaScript
library bevacqua/fuzzysearch.")
    (license license:expat)))

(define-public waybar-mpris
  (package
    (name "waybar-mpris")
    (version "0.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/arafatamim/waybar-mpris.git")
                    (commit "a10ff4c12de19cb4d776cd990300bd16f0d570b4")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0babh4bfnvg9gykrggyw7nabwydwqcc1d0w7p7w8zv5r3gq1ss5h"))))
    (build-system go-build-system)
    (propagated-inputs (list go-github-com-fsnotify-fsnotify go-github-com-spf13-pflag go-github-com-godbus-dbus go-github-com-arafatamim-mpris2client))
    (arguments
     (list #:import-path "github.com/arafatamim/waybar-mpris"
           #:tests? #f ; Source-only package.
           ;; #:phases
           ;; #~(modify-phases %standard-phases
           ;;     ;; Source-only package.
           ;;     (delete 'build))
           ))
    (home-page "https://github.com/lithammer/fuzzysearch")
    (synopsis "Tiny and fast fuzzy search in Go")
    (description
     "A speedy fuzzy matching package for Go inspired by the JavaScript
library bevacqua/fuzzysearch.")
    (license license:expat)))
waybar-mpris

