;;; Modified from Gnu Guix

;;; GNU Guix is free software; you can redistribute it and$
;;; under the terms of the GNU General Public License as p$
;;; the Free Software Foundation; either version 3 of the $
;;; your option) any later version.

(define-module (waybar-with-cava)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system asdf)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system haskell)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system perl)
  #:use-module (guix build-system python)
  #:use-module (guix build-system trivial)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages calendar)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages check)
  #:use-module (gnu packages datastructures)
  #:use-module (gnu packages docbook)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages fribidi)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gperf)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages haskell-check)
  #:use-module (gnu packages haskell-web)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages image)
  #:use-module (gnu packages imagemagick)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages lisp-check)
  #:use-module (gnu packages lisp-xyz)
  #:use-module (gnu packages logging)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages man)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpd)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages pciutils)
  #:use-module (gnu packages music)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pretty-print)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages readline)
  #:use-module (gnu packages samba)  
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages suckless)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages textutils)
  #:use-module (gnu packages time)
  #:use-module (gnu packages video)
  #:use-module (gnu packages web)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg))

(define-public cava-lib
  (package
    (name "cava-lib")
    (version "0.8.5")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/LukashonakV/cava")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "06l0dsx4g4s7jmv59fwiinkc2nwla6j581nbsys7agkwp2ldzxbg")))) ;; version 0.8.5, which fails
		;; "0hi5cam7gfyziplnlf1mfq8j263ggqxib8rl79bmz29b4789razb")))) ;; version 0.8.4, which fails
		;;"")))) ;; version 0.8.4, which fails
    (build-system meson-build-system)
    (native-inputs (list autoconf automake libtool pkg-config))
    (inputs (list fftw ncurses iniparser pulseaudio portaudio pipewire ncurses tinyalsa)) ;; re-add portaudio and remove ncurses if needed to make waybar work
    
    ;; TODO: this is terible code below, but it works.
    ;; there is almost certainly cruft that can be removed, but not
    ;; sure where.
    (arguments
     (list #:configure-flags
	   #~(list (string-append "DESTDIR=" #$output)
		   (string-append "FONTDIR="
				  #$output "/share/consolefonts"))
	   #:phases
	   #~(modify-phases %standard-phases
			    (add-before 'configure 'autogen
					(lambda _
					  (setenv "HOME"
						  (getcwd))
					  (invoke "sh" "autogen.sh")))
			    (replace 'configure
			     	     (lambda _
			     	       (let ((destdir #$output))
					 (setenv "DESTDIR"
						  destdir)
			     		 (invoke "meson" "setup" "build"))))
			    ;; (add-before 'build 'make-cava-ldflags
			    ;; 		(lambda _
			    ;; 		  (mkdir-p (string-append #$output "/lib"))))
			    (replace 'build
				     (lambda _
				       (let ((destdir #$output))
					 (setenv "DESTDIR"
						 destdir)
					 (invoke "meson" "compile" "-C" "build"))))
			    (replace 'install
				     (lambda _
				       (let ((destdir #$output))
					 (setenv "DESTDIR"
						 destdir)
					 (setenv "PREFIX"
						 destdir)					 
					 (invoke "meson" "install" "-C" "build"))))
			    (add-after 'install 'data
				       (lambda _
					 (for-each (lambda (file)
						     (install-file file
								   (string-append #$output
										  "/share/doc/examples")))
						   (find-files "example_files"))))
			    (add-after 'install 'fix-output-dirs
				       (lambda _
					 (let ((destdir #$output))
					   (invoke "sh" "-c" (string-append "mv " destdir "/usr/local/lib " destdir))
					   (invoke "sh" "-c" (string-append "mv " destdir "/usr/local/include " destdir)))))
			    (delete 'check))))
    (home-page "https://github.com/karlstav/cava")
    (synopsis "Console audio visualizer for ALSA, MPD, and PulseAudio")
    (description "C.A.V.A. is a bar audio spectrum visualizer for the terminal
using ALSA, MPD, PulseAudio, or a FIFO buffer as its input.")
    (license license:expat)))

(define-public waybar-with-cava
  (package
    (name "waybar-with-cava")
    (version "0.9.20")
     (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/Alexays/Waybar")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 ;;"1gjg7phrziz3lnmz6l9dhr3icxcd41kyyjnpn1w629pdmym4z5p7")))) 0.9.19
	 "07h5l7h7wmzqgg7fbp98khrxg2sq2s4ncp4fiiz1yg62r752idy4")))) ;; 0.9.20, works
	 	 ;;"0f902svy4vrx3hh0s0vhpmyzhbnfc4ai7asjwqxrxf4cim317x2n")))) ;; 0.9.21 fails on ninja -j 4 with /gnu/store/cbjgz6f8nrb7804nnmmlvpd4y78p8zf3-glibmm-2.64.5/include/glibmm-2.4/glibmm/variant.h:1751:1: error: no declaration matches ‘Glib::Variant<Glib::Variant<T> >::Variant()’
    (build-system meson-build-system)
    (inputs (list cava-lib
		  cava
		  date
                  fmt
		  fftw ;; needed when compiling with cavalib
                  gtk-layer-shell
                  gtkmm-3
		  ;;glibmm-next ;; or --with-input=glibmm=glibmm@2.76.0, but that fails to rebuild atkmm which needs glibmm@2.4
                  glibmm
		  ;; 
                  jsoncpp
                  libdbusmenu
                  libinput-minimal
                  libmpdclient
                  libnl
                  libxml2
                  playerctl
                  pulseaudio
                  spdlog
                  wayland))
    (native-inputs
     (list `(,glib "bin") pkg-config scdoc wayland-protocols))
    (home-page "https://github.com/Alexays/Waybar")
    (synopsis "Wayland bar for Sway and Wlroots based compositors")
    (description "Waybar is a highly customisable Wayland bar for Sway and
Wlroots based compositors.")
    (license license:expat))) ; MIT license   
waybar-with-cava
;;cava-lib
